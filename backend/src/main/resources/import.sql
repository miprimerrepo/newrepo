/* Populate tables */
INSERT INTO entrenadores (nombre, apellido, email,telefono,pais,region,foto,fecha_nac) VALUES('pacco', 'Guzman', 'profesornew@bolsadeideas.com','55055','Chile','Metropolitana','Sin Foto', '1987-08-01');
INSERT INTO entrenadores (nombre, apellido, email,telefono,pais,region,foto,fecha_nac) VALUES('luisa', 'Rodriguez', 'profesor12@bolsadeideas.com','55055','Chile','Maule','Sin Foto', '1990-08-11');
INSERT INTO entrenadores (nombre, apellido, email,telefono,pais,region,foto,fecha_nac) VALUES('dorian', 'Morales', 'profesor14@bolsadeideas.com','55055','Chile','Maule','Sin Foto', '1850-03-21');
INSERT INTO entrenadores (nombre, apellido, email,telefono,pais,region,foto,fecha_nac) VALUES('ricardo', 'Easwood', 'profesor143@bolsadeideas.com','55055','Chile','Metropolitana','Sin Foto', '1999-04-22');
INSERT INTO entrenadores (nombre, apellido, email,telefono,pais,region,foto,fecha_nac) VALUES('hash', 'ketchut', 'entrenadornew@bolsadeideas.com','55055','Chile','Metropolitana','Sin Foto', '1999-02-02');

INSERT INTO pokemons (descripcion,foto,nombre,tipo) VALUES('Pokemon como gato','Sin Foto','Pikachu','Electrico');
INSERT INTO pokemons (descripcion,foto,nombre,tipo) VALUES('Pokemon como Perro','Sin Foto','Perrin','Domestico');
INSERT INTO pokemons (descripcion,foto,nombre,tipo) VALUES('Pokemon como Perro','Sin Foto','Start','Acuatico');
INSERT INTO pokemons (descripcion,foto,nombre,tipo) VALUES('Pokemon como Perro','Sin Foto','Llama','Fuego');
INSERT INTO pokemons (descripcion,foto,nombre,tipo) VALUES('Pokemon extra�o','Sin Foto','Peluchin','Domestico');
INSERT INTO pokemons (descripcion,foto,nombre,tipo) VALUES('Pokemon docil','Sin Foto','Gato','Domestico');


INSERT INTO entrenadorespokemons (fecha_captura,sexo_pokemon,entrenador_id,pokemon_id) VALUES('2018-01-01','Femenino','1','2');
INSERT INTO entrenadorespokemons (fecha_captura,sexo_pokemon,entrenador_id,pokemon_id) VALUES('2018-01-01','Femenino','1','1');
INSERT INTO entrenadorespokemons (fecha_captura,sexo_pokemon,entrenador_id,pokemon_id) VALUES('2018-01-01','Femenino','1','3');
INSERT INTO entrenadorespokemons (fecha_captura,sexo_pokemon,entrenador_id,pokemon_id) VALUES('2018-01-01','Femenino','1','4');
INSERT INTO entrenadorespokemons (fecha_captura,sexo_pokemon,entrenador_id,pokemon_id) VALUES('2018-01-01','Femenino','1','5');
INSERT INTO entrenadorespokemons (fecha_captura,sexo_pokemon,entrenador_id,pokemon_id) VALUES('2018-01-01','Femenino','2','2');
INSERT INTO entrenadorespokemons (fecha_captura,sexo_pokemon,entrenador_id,pokemon_id) VALUES('2018-01-01','Femenino','2','3');
INSERT INTO entrenadorespokemons (fecha_captura,sexo_pokemon,entrenador_id,pokemon_id) VALUES('2018-01-01','Femenino','3','1');
INSERT INTO entrenadorespokemons (fecha_captura,sexo_pokemon,entrenador_id,pokemon_id) VALUES('2018-01-01','Femenino','4','4');
INSERT INTO entrenadorespokemons (fecha_captura,sexo_pokemon,entrenador_id,pokemon_id) VALUES('2018-01-01','Femenino','4','5');
