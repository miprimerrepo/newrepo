package com.pokemon.app.models.services;

import java.util.List;

import com.pokemon.app.models.entity.Entrenador;

public interface IEntrenadorService {

	public Entrenador finOneById(Long id);
	public List<Entrenador> finAll();
	public Entrenador save(Entrenador entrenador);
	public void deleteById(Long id);
}
