package com.pokemon.app.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pokemon.app.models.dao.IEntrenadorPokemonDao;
import com.pokemon.app.models.entity.EntrenadorPokemon;

@Service
public class EntrenadorPokemonServiceImpl implements IEntrenadorPokemonService {

	@Autowired
	IEntrenadorPokemonDao claseDao;
	
	@Override
	@Transactional(readOnly = true)
	public EntrenadorPokemon finOneById(Long id) {
		// TODO Auto-generated method stub
		return claseDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<EntrenadorPokemon> finAll() {
		// TODO Auto-generated method stub
		return (List<EntrenadorPokemon>) claseDao.findAll();
	}

	@Override
	@Transactional
	public EntrenadorPokemon save(EntrenadorPokemon entrenadorPokemon) {
		// TODO Auto-generated method stub
		return claseDao.save(entrenadorPokemon);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		claseDao.deleteById(id);
	}

}
