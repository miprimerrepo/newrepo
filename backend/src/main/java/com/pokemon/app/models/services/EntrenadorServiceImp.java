package com.pokemon.app.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pokemon.app.models.dao.IEntrenadorDao;
import com.pokemon.app.models.entity.Entrenador;

@Service
public class EntrenadorServiceImp implements IEntrenadorService {

	@Autowired
	IEntrenadorDao entrenadorDao;
	
	@Override
	@Transactional(readOnly = true)
	public Entrenador finOneById(Long id) {
		// recibe un id,Busca un Registro por id en la tabla entrenadores, retorna un objeto entrenador
		return entrenadorDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Entrenador> finAll() {
		// TODO Auto-generated method stub
		return (List<Entrenador>) entrenadorDao.findAll();
	}

	@Override
	@Transactional
	public Entrenador save(Entrenador entrenador) {
		// TODO Auto-generated method stub
		return entrenadorDao.save(entrenador);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
	entrenadorDao.deleteById(id);
	}

}
