package com.pokemon.app.models.services;

import java.util.List;

import com.pokemon.app.models.entity.Pokemon;

public interface IPokemonService {

	public Pokemon finOneById(Long id);
	public List<Pokemon> finAll();
	public Pokemon save(Pokemon pokemon);
	public void deleteById(Long id);
}
