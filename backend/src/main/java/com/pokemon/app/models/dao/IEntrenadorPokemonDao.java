package com.pokemon.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.pokemon.app.models.entity.EntrenadorPokemon;

public interface IEntrenadorPokemonDao extends CrudRepository<EntrenadorPokemon, Long> {

}
