package com.pokemon.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.pokemon.app.models.entity.Entrenador;

public interface IEntrenadorDao extends CrudRepository<Entrenador, Long>{

}
