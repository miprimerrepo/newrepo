package com.pokemon.app.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "pokemons")
public class Pokemon implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message = "No Puedo Estar Vacío")
	@Size(min = 2,max = 20, message = "Debe Contener Entre 2 a 20 Caracteres")
	@Column(name = "nombre",nullable = false)
	private String nombre;
	
	@NotEmpty(message = "No Puedo Estar Vacío")
	@Size(min = 2,max = 20, message = "Debe Contener Entre 2 a 20 Caracteres")
	@Column(name = "foto",nullable = false)
	private String foto;
	
	@NotEmpty(message = "No Puedo Estar Vacío")
	@Size(min = 2,max = 20, message = "Debe Contener Entre 2 a 20 Caracteres")
	@Column(name = "descripcion",nullable = false)
	private String descripcion;
	
	@NotEmpty(message = "No Puedo Estar Vacío")
	@Size(min = 2,max = 20, message = "Debe Contener Entre 2 a 20 Caracteres")
	@Column(name = "tipo",nullable = false)
	private String tipo;
	
	
	@OneToMany(mappedBy = "pokemon",fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true)
	private List<EntrenadorPokemon> entrenadorPokemons;
	
	
	
	public Pokemon() {
		entrenadorPokemons=new ArrayList<>();
	}

	public void addEntrenadorPokemons(EntrenadorPokemon entrenadorPokemon) {
		entrenadorPokemons.add(entrenadorPokemon);
	}
	
	public List<EntrenadorPokemon> getEntrenadorPokemons() {
		return entrenadorPokemons;
	}


	public void setEntrenadorPokemons(List<EntrenadorPokemon> entrenadorPokemons) {
		this.entrenadorPokemons = entrenadorPokemons;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getFoto() {
		return foto;
	}


	public void setFoto(String foto) {
		this.foto = foto;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
}
