package com.pokemon.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.pokemon.app.models.entity.Pokemon;

public interface IPokemonDao extends CrudRepository<Pokemon, Long> {

}
