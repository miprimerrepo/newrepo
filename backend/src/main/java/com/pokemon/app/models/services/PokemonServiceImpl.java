package com.pokemon.app.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pokemon.app.models.dao.IPokemonDao;
import com.pokemon.app.models.entity.Pokemon;

@Service
public class PokemonServiceImpl implements IPokemonService {

	@Autowired
	IPokemonDao pokemonDao;
	
	@Override
	@Transactional(readOnly = true)
	public Pokemon finOneById(Long id) {
		// TODO Auto-generated method stub
		return pokemonDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Pokemon> finAll() {
		// TODO Auto-generated method stub
		return (List<Pokemon>) pokemonDao.findAll();
	}

	@Override
	@Transactional
	public Pokemon save(Pokemon pokemon) {
		// TODO Auto-generated method stub
		return pokemonDao.save(pokemon);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		pokemonDao.deleteById(id);
	}

}
