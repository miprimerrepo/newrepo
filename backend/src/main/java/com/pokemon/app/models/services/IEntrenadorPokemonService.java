package com.pokemon.app.models.services;

import java.util.List;

import com.pokemon.app.models.entity.EntrenadorPokemon;

public interface IEntrenadorPokemonService {

	public EntrenadorPokemon finOneById(Long id);
	public List<EntrenadorPokemon> finAll();
	public EntrenadorPokemon save(EntrenadorPokemon entrenadorPokemon);
	public void deleteById(Long id);
}
