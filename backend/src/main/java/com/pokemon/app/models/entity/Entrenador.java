package com.pokemon.app.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "entrenadores")
public class Entrenador implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message = "No Puedo Estar Vacío")
	@Size(min = 2,max = 20, message = "Debe Contener Entre 2 a 20 Caracteres")
	@Column(name = "nombre",nullable = false)
	private String nombre;
		
	@NotEmpty(message = "No Puedo Estar Vacío")
	@Size(min = 2,max = 20, message = "Debe Contener Entre 2 a 20 Caracteres")
	@Column(name = "apellido",nullable = false)
	private String apellido;
	
	@NotEmpty(message = "No Puedo Estar Vacío")
	@Email(message = "El Correo Tiene Un Formato Invalido")
	@Column(name = "email",nullable = false,unique = true)
	private String email;
	
	@NotEmpty(message = "No Puedo Estar Vacío")
	@Size(min = 5,max = 20, message = "Debe Contener Entre 5 a 20 Caracteres")
	@Column(name = "telefono",nullable = false)
	private String telefono;
	
	@NotEmpty(message = "No Puedo Estar Vacío")
	@Size(min = 4,max = 20, message = "Debe Contener Entre 4 a 20 Caracteres")
	@Column(name = "pais",nullable = false)
	private String pais;
	
	@NotEmpty(message = "No Puedo Estar Vacío")
	@Size(min = 4,max = 20, message = "Debe Contener Entre 4 a 20 Caracteres")
	@Column(name = "region",nullable = false)
	private String region;
	
	@NotEmpty(message = "No Puedo Estar Vacío")
	@Size(min = 2,max = 20, message = "Debe Contener Entre 2 a 20 Caracteres")
	@Column(name = "foto",nullable = false)
	private String foto;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@Column(name = "fecha_nac")
	private Date fechaNac;
	
	@OneToMany(mappedBy = "entrenador",fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true)
	private List<EntrenadorPokemon> entrenadorPokemons;
	
	public void addEntrenadorPokemons(EntrenadorPokemon entrenadorPokemon) {
		entrenadorPokemons.add(entrenadorPokemon);
	}
	
	public List<EntrenadorPokemon> getEntrenadorPokemons() {
		return entrenadorPokemons;
	}

	

	
	public Entrenador() {
		entrenadorPokemons=new ArrayList<>();
	}

	public void setEntrenadorPokemons(List<EntrenadorPokemon> entrenadorPokemons) {
		this.entrenadorPokemons = entrenadorPokemons;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getApellido() {
		return apellido;
	}



	public void setApellido(String apellido) {
		this.apellido = apellido;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getTelefono() {
		return telefono;
	}



	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}



	public String getPais() {
		return pais;
	}



	public void setPais(String pais) {
		this.pais = pais;
	}



	public String getRegion() {
		return region;
	}



	public void setRegion(String region) {
		this.region = region;
	}



	public String getFoto() {
		return foto;
	}



	public void setFoto(String foto) {
		this.foto = foto;
	}



	public Date getFechaNac() {
		return fechaNac;
	}



	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
}
