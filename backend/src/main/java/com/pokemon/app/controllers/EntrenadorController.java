package com.pokemon.app.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pokemon.app.models.entity.Entrenador;
import com.pokemon.app.models.services.IEntrenadorService;

@RestController
@CrossOrigin({ "http://localhost:4200" })
@RequestMapping("/api")
public class EntrenadorController {

	@Autowired
	IEntrenadorService entrenadorService;

	@GetMapping("/entrenador")
	@ResponseStatus(HttpStatus.OK)
	public List<Entrenador> findAll() {
		return entrenadorService.finAll();
	}

	@GetMapping("/entrenador/{id}")
	public ResponseEntity<?> findOneById(@PathVariable(name = "id") Long id) {
		Entrenador entrenador = null;
		Map<String, Object> response = new HashMap<>();
		try {
			entrenador = entrenadorService.finOneById(id);
		} catch (DataAccessException e) {
			response.put("Mensaje", "Error En la Consulta de Busqueda");
			response.put("error", e.getMessage().concat("= ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (entrenador == null) {
			response.put("Mensaje", "El Entrenador No Existe");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Entrenador>(entrenador, HttpStatus.OK);
	}

	@PostMapping("/entrenador")
	public ResponseEntity<?> save(@Valid @RequestBody Entrenador entrenador, BindingResult result) {
		Map<String, Object> response = new HashMap<>();
		Entrenador entrenadorNew;

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El Campo ".concat(err.getField()).concat(" ").concat(err.getDefaultMessage()))
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			entrenadorNew = entrenadorService.save(entrenador);
		} catch (DataAccessException e) {
			response.put("Mensaje", "Error Al Intentar Guardar Los Datos");
			response.put("error", e.getMessage().concat("= ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "Datos Guardados Con Exito!");
		response.put("entrenador", entrenadorNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PutMapping("/entrenador/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Entrenador entrenador, BindingResult result,
			@PathVariable(name = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		Entrenador entrenadorNew;
		Entrenador entrenadorbd = entrenadorService.finOneById(id);
		if (entrenadorbd == null) {
			response.put("Mensaje", "El Entrenador No Existe");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El Campo ".concat(err.getField()).concat(" ").concat(err.getDefaultMessage()))
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			entrenadorbd.setApellido(entrenador.getApellido());
			entrenadorbd.setNombre(entrenador.getNombre());
			entrenadorbd.setEmail(entrenador.getEmail());
			entrenadorbd.setTelefono(entrenador.getTelefono());
			entrenadorbd.setPais(entrenador.getPais());
			entrenadorbd.setRegion(entrenador.getRegion());
			entrenadorbd.setFoto(entrenador.getFoto());
			entrenadorbd.setFechaNac(entrenador.getFechaNac());
			entrenadorNew = entrenadorService.save(entrenadorbd);
		} catch (DataAccessException e) {
			response.put("Mensaje", "Error Al Intentar Actualizar Los Datos");
			response.put("error", e.getMessage().concat("= ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "Datos Modificados Con Exito!");
		response.put("entrenador", entrenadorNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@DeleteMapping("/entrenador/{id}")
	public ResponseEntity<?> delete(@PathVariable(name = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		Entrenador entrenadorbd = entrenadorService.finOneById(id);
		if (entrenadorbd == null) {
			response.put("Mensaje", "El Entrenador No Existe");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			entrenadorService.deleteById(id);
		} catch (DataAccessException e) {
			response.put("Mensaje", "Error Al Intentar Actualizar Los Datos");
			response.put("error", e.getMessage().concat("= ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "Entrenador Borrado Con Exito!");
		response.put("entrenador", entrenadorbd);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
