package com.pokemon.app.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.pokemon.app.models.entity.Pokemon;
import com.pokemon.app.models.services.IPokemonService;

@RestController
@CrossOrigin({ "http://localhost:4200" })
@RequestMapping("/api")
public class PokemonController {

	@Autowired
	IPokemonService pokemonSerivice;

	@GetMapping("/pokemon")
	@ResponseStatus(HttpStatus.OK)
	public List<Pokemon> findAll() {
		return pokemonSerivice.finAll();
	}

	@GetMapping("/pokemon/{id}")
	public ResponseEntity<?> finOneById(@PathVariable(name = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		Pokemon pokemon = null;
		try {
			pokemon = pokemonSerivice.finOneById(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error En la Colsulta");
			response.put("error", e.getMessage().concat(" =").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (pokemon == null) {
			response.put("mensaje", "El Pokemon No Existe en la BD");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Pokemon>(pokemon, HttpStatus.OK);
	}

	@PostMapping("/pokemon")
	public ResponseEntity<?> save(@Valid @RequestBody Pokemon pokemon, BindingResult result) {
		Map<String, Object> response = new HashMap<>();
		Pokemon pokemonnew = null;

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El Campo ".concat(err.getField()).concat(" ").concat(err.getDefaultMessage()))
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			pokemonnew = pokemonSerivice.save(pokemon);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al intentar Guardar los Datos");
			response.put("error", e.getMessage().concat(" =").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "Pokemon Guardado con Exito!");
		response.put("pokemonnew", pokemonnew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PutMapping("/pokemon/{id}")
	public ResponseEntity<?> update(@RequestBody Pokemon pokemon, BindingResult result,
			@PathVariable(name = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		Pokemon pokemonnew = null;
		Pokemon poNew = pokemonSerivice.finOneById(id);
		if (poNew == null) {
			response.put("mensaje", "El Pokemon No Existe en la BD");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El Campo ".concat(err.getField()).concat(" ").concat(err.getDefaultMessage()))
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			poNew.setDescripcion(pokemon.getDescripcion());
			poNew.setNombre(pokemon.getNombre());
			poNew.setFoto(pokemon.getFoto());
			poNew.setTipo(pokemon.getTipo());
			pokemonnew = pokemonSerivice.save(poNew);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al intentar Actualziar los Datos");
			response.put("error", e.getMessage().concat(" =").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "Pokemon Editado con Exito!");
		response.put("pokemonnew", pokemonnew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
