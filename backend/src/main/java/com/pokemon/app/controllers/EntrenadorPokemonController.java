package com.pokemon.app.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.pokemon.app.models.entity.EntrenadorPokemon;
import com.pokemon.app.models.services.EntrenadorPokemonServiceImpl;

@RestController
@CrossOrigin({ "http://localhost:4200" })
@RequestMapping("/api")
public class EntrenadorPokemonController {

	@Autowired
	EntrenadorPokemonServiceImpl service;

	@GetMapping("/entrenadorpokemon")
	@ResponseStatus(HttpStatus.OK)
	public List<EntrenadorPokemon> findAll() {
		return service.finAll();
	}

	@GetMapping("/entrenadorpokemon/{id}")
	public ResponseEntity<?> findOneById(@PathVariable(name = "id") Long id) {
		EntrenadorPokemon entrenadorP = null;
		Map<String, Object> response = new HashMap<>();
		try {
			entrenadorP = service.finOneById(id);
		} catch (DataAccessException e) {
			response.put("Mensaje", "Error En la Consulta de Busqueda");
			response.put("error", e.getMessage().concat("= ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (entrenadorP == null) {
			response.put("Mensaje", "El Entrenador No Existe");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<EntrenadorPokemon>(entrenadorP, HttpStatus.OK);
	}

	@PostMapping("/entrenadorpokemon")
	public ResponseEntity<?> save(@Valid @RequestBody EntrenadorPokemon entrenadorP, BindingResult result) {
		Map<String, Object> response = new HashMap<>();
		EntrenadorPokemon entrenadorNewP;

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El Campo ".concat(err.getField()).concat(" ").concat(err.getDefaultMessage()))
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			entrenadorNewP = service.save(entrenadorP);
		} catch (DataAccessException e) {
			response.put("Mensaje", "Error Al Intentar Guardar Los Datos");
			response.put("error", e.getMessage().concat("= ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "Datos Guardados Con Exito!");
		response.put("entrenador", entrenadorNewP);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PutMapping("/entrenadorpokemon/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody EntrenadorPokemon entrenadorP, BindingResult result,
			@PathVariable(name = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		EntrenadorPokemon entrenadorNewP;
		EntrenadorPokemon entrenadorbdP = service.finOneById(id);
		if (entrenadorbdP == null) {
			response.put("Mensaje", "El Entrenador No Existe");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		if (result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream()
					.map(err -> "El Campo ".concat(err.getField()).concat(" ").concat(err.getDefaultMessage()))
					.collect(Collectors.toList());
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			entrenadorbdP.setEntrenador(entrenadorP.getEntrenador());
			entrenadorbdP.setFechaCaptura(entrenadorP.getFechaCaptura());
			entrenadorbdP.setPokemon(entrenadorP.getPokemon());
			entrenadorbdP.setSexoPokemon(entrenadorP.getSexoPokemon());

			entrenadorNewP = service.save(entrenadorbdP);
		} catch (DataAccessException e) {
			response.put("Mensaje", "Error Al Intentar Actualizar Los Datos");
			response.put("error", e.getMessage().concat("= ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "Datos Modificados Con Exito!");
		response.put("entrenador", entrenadorNewP);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@DeleteMapping("/entrenadorpokemon/{id}")
	public ResponseEntity<?> delete(@PathVariable(name = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		EntrenadorPokemon entrenadorbdP = service.finOneById(id);
		if (entrenadorbdP == null) {
			response.put("Mensaje", "El Entrenador No Existe");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			service.deleteById(id);
		} catch (DataAccessException e) {
			response.put("Mensaje", "Error Al Intentar Actualizar Los Datos");
			response.put("error", e.getMessage().concat("= ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "Entrenador Borrado Con Exito!");
		response.put("entrenador", entrenadorbdP);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
